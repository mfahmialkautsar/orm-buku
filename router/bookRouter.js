const router = require('express').Router();
const bookController = require('../controller/bookController');

router.get('/', bookController.index);
router.post('/', bookController.create);
router.get('/:id', bookController.show);
router.put('/:id', bookController.update);
router.delete('/:id', bookController.destroy);

module.exports = router;
