const express = require('express');
const app = express();
const { PORT = 3000 } = process.env;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const router = require('./router');
app.use(router);

app.listen(PORT, () => console.log(`http://localhost:${PORT}`));
