const { books: Books } = require('../models');

module.exports = {
  index: async (req, res) => {
    try {
      const books = await Books.findAll();
      res.json({
        status: 200,
        data: books,
      });
    } catch (error) {
      res.send(error.message);
    }
  },
  create: async (req, res) => {
    try {
      const books = await Books.create(req.body);
      res.statusCode = 201;
      res.json({
        results: books,
      });
    } catch (error) {
      res.send(error.message);
    }
  },
  show: async (req, res) => {
    try {
      const id = req.params.id;
      const query = {
        where: { id },
      };

      const result = await Books.findOne(query);

      if (result == 1) {
        res.json({
          message: 'Sukses',
        });
      }
      throw new Error('Error');
    } catch (error) {
      res.send(error.message);
    }
  },
  update: async (req, res) => {
    try {
      const id = req.params.id;
      const query = {
        where: { id },
      };

      const result = await Books.update(req.body, query);

      if (result == 1) {
        res.json({
          message: 'Sukses',
        });
      }
      throw new Error('Error');
    } catch (error) {
      res.send(error.message);
    }
  },
  destroy: async (req, res) => {
    try {
      const id = req.params.id;
      const query = {
        where: { id },
      };
      const result = await Books.destroy(query);

      if (result) {
        res.json({
          message: 'Sukses',
        });
      }
      throw new Error('Error');
    } catch (error) {
      res.send(error.message);
    }
  },
};
